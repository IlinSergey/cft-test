from sqlalchemy import select
from sqlalchemy.ext.asyncio import async_sessionmaker, create_async_engine
from sqlalchemy.orm import selectinload

from auth.database import DATABASE_URL, User

engine = create_async_engine(DATABASE_URL)
async_session = async_sessionmaker(engine, expire_on_commit=False)


async def get_salary(user_email):
    async with async_session() as session:
        async with session.begin():
            stmt = select(User).options(selectinload(User.salary)).where(User.email == user_email)
            result = await session.scalars(stmt)
            for i in result:
                salary = float("{:.2f}".format(i.salary.salary))
                date = i.salary.date_increase.strftime("%Y-%m-%d")
                return f"Текущий оклад {salary} руб. Дата следующего повышения {date}"
